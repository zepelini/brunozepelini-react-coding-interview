import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import { ContactInlineEdit } from './contactInlineEdit';

export interface IContactCardProps {
  person: IContact;
  onSave: (currentText: string) => void;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  onSave,
  sx,
}) => {
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <ContactInlineEdit label="Name" text={name} onSave={onSave} />
          <ContactInlineEdit label="E-mail" text={email} onSave={onSave}  />
        </Box>
      </Box>
    </Card>
  );
};
