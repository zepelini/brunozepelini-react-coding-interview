import { TextField, Button } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';
import { InlineEdit } from '@components/atoms/inline-edit';
import { ChangeEvent, useState } from 'react';

export interface IContactCardProps {
  text: string;
  label: string;
  onSave: (currentText: string) => void;
  sx?: SystemStyleObject<Theme>;
}

export const ContactInlineEdit: React.FC<IContactCardProps> = ({ text, label, sx, onSave }) => {

  const [currentText, setCurrentText] = useState<string>(text);
  const [writingMode, setWritingMode] = useState<boolean>(false);

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    setCurrentText(event.currentTarget.value);
  }

  const onUpdateContactText = () => {
    setWritingMode(false);
    onSave(currentText);
  }

  return (
    <InlineEdit sx={sx}>
      {
        !writingMode && <Button onClick={() => setWritingMode(true) }>{currentText}</Button>
      }
      {
        writingMode && <>
          <TextField label={label} value={currentText} onChange={onChange} />
          <Button onClick={onUpdateContactText}>OK</Button>
          <Button onClick={() => setWritingMode(false) }>Nope</Button>
        </>
      }
    </InlineEdit>
  );
};
